import styled from "styled-components";

export const Basic = styled.div`
  font-size: 1.5em;
  text-align: center;
  color: palevioletred;
  .btn-lg{
      border-radius:50px;
  }
`;
