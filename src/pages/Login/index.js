import { Button } from "@material-ui/core";
import React,{ useEffect, useState } from "react";
import { Basic } from "./style";

function Login(){
        const [loggedin, setLoggedin] = useState(false);
        const onClick = () => {
          setLoggedin(!loggedin);
        };
        useEffect(() => {
          document.title = loggedin ? `You loggined` : `You logout`;
        }, [loggedin]);

    return (
      <Basic>
        <main className="kd_bg default-transition" style={{ opacity: 1 }}>
          <div className="left-container">
            <img
              src="https://performance.khojdeal.com/img/logo-khojdeal-inverted.png"
              alt="khojdeal performance portal"
            />
          </div>
          <div className="flex-d">
            {/* <GoogleLogin
              clientId="1037454523551-lp69q0glet6qcgku36j02qoqmtk6ucfj.apps.googleusercontent.com"
              render={(renderProps) => (
                <button
                  onClick={renderProps.onClick}
                  className="btn btn-danger btn-lg mb-1"
                  disabled={renderProps.disabled}
                >
                  Login With Gmail
                </button>
              )}
              buttonText="Login"
              cookiePolicy={"single_host_origin"}
            /> */}
            <Button
              onClick={onClick}
              variant="contained"
              className="btn btn-danger btn-lg mb-1"
            >
              Login with Gmail
            </Button>
          </div>
        </main>
      </Basic>
    );
}
export default Login;