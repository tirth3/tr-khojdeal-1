import styled from "styled-components";

export const Lay = styled.div`
  max-width: 1600px;
  margin:auto;
  background-image: url("https://performance.khojdeal.com/img/back.png");
  background-size: contain;
  background-repeat: no-repeat;
  .tr-flex {
    display: flex;
    margin-top: 20px;
    margin-bottom: 45px;
  }
  .tr-flex > *:nth-child(1) {
    flex: 1 1 auto;
  }
  .tr-flex > *:nth-child(2) {
    flex: 1 1 85%;
  }
`;
