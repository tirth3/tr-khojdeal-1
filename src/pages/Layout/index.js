import Header from "../../Components/Header";
import Section from "../../Components/Section";
import Sidebar from "../../Components/Sidebar";
import React,{ useState } from 'react';
import { Lay } from "./style";
import Model from "../../Components/Model";
import Chart from "../../Components/Chart";


function Layout(){
  const [togglevalue,setToggleValue]=useState();
  const [togglemodel,setToggleModel]=useState();
  const getToggle=(toggle)=>{
    setToggleValue(toggle);
  }
  const getModel=(model)=>{
    setToggleModel(model);
  }

    return (
      <Lay>
        <Header getToggle={getToggle} getModel={getModel}/>
        <div className="tr-flex">
          <Sidebar togglevalue={togglevalue}/>
          <Section />
          <Model togglemodel={togglemodel}/>
        </div>
        <Chart/>
      </Lay>
    );
}
export default Layout;