import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Layout from '.';

export default {
  title: 'Layout',
  component: Layout,
  // argTypes: { onClick: { action: () =>  alert("Clicked")} },
};
const Template = (args) => <BrowserRouter><Layout {...args}/></BrowserRouter>;
export const Container = Template.bind({});
// Container.args = {
// togglemodel:false,
// };