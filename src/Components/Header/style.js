import styled from "styled-components";

export const Head = styled.div`
  background-color: white;
  position: sticky;
  top: 0;
  z-index:99;
  padding: 5px 10px;
  box-shadow: rgb(0 0 0 / 20%) 0px 3px 3px -2px,
    rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px;
  .toggle-bar {
    right: 13px;
    position: relative;
    top: 15px;
  }
  .mx-3{
    padding-left:3vw;
    padding-right:3vw;
  }
  .collapse-btn div {
    width: 25px;
    height: 5px;
    background-color: black;
    margin-bottom: 2px;
  }
  .nav-flex {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  }
`;
