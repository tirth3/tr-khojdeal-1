import React from 'react';

import Header from '.';

export default {
  title: 'Header',
  component: Header,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <Header {...args} />;

export const Head = Template.bind({});
Head.args = {
  getToggle:true,
  getModel:true,
};