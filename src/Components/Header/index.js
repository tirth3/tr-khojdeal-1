import React from "react";
import { Head } from "./style";
import { useState} from 'react';
import Pen from '../../assets/pen.png';
import { Button } from "@material-ui/core";

function Header({getToggle,getModel}){
  const [toggle,setToggle]=useState(false);
  const [model,setModel]=useState(false);

  const handleChange = (event) => {
    setToggle(!toggle)
    getToggle(!toggle);
  };
  const handleModel=(res)=>{
    setModel(!model)
    getModel(!model);
  }
  return (
    <Head>
      <div className="nav-flex">
        <div className="nav-flex">
          <Button
            className="mx-3"
            // className={
            //   toggle === true
            //     ? "bg-selected tr-toggle-true tr-radius"
            //     : "bg-normal tr-toggle-false tr-radius","toggle-bar"
            // }
            onClick={handleChange}
          >
            <div className="collapse-btn">
              <div></div>
              <div></div>
              <div></div>
            </div>
          </Button>
          <img
            src="https://assets.khojdeal.com/assets/image/Logo.svg"
            alt=""
            width="180px"
            height="50px"
          />
        </div>
        <div>
          <Button variant="contained" color="primary"  onClick={handleModel}>
            <img src={Pen} alt="" height="25px" width="25px" />
          </Button>
        </div>
      </div>
    </Head>
  );
}
export default Header;