import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import React from "react";

export default function ChartComponent({data}){
return(
  <ResponsiveContainer width="95%" height={400}>
<LineChart
      width={300}
      height={100}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="pv"
        stroke="#8884d8"
        activeDot={{ r: 1 }}
        strokeWidth={4}
        strokeDasharray="6 2"
        dot={false}
      />
      <Line type="monotone" dataKey="uv" stroke="#82ca9d" strokeWidth={4} activeDot={{ r: 1 }} dot={false}/>
    </LineChart>
    </ResponsiveContainer>
    )}