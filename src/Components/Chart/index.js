import { Typography } from "@material-ui/core";
import {Container} from './styles'
import React, { useState } from "react";
import ChartComponent from "./ChartComponent";
import BarChartComponent from "./BarChartComponent";

const data1 = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
];

const data2 = [
    {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
];

const data3 = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const data4 = [
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
    {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
    {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
];

export default function Chart() {
    const [tab, setTab] = useState(1);
    const [chart,setChart]=useState(false);
  return (   
      <Container>
      <div className="tab">
        <div>
        <button
          className={tab === 1 ? 'bg-selected width1' : 'width1'}
          onClick={() => setTab(1)}
        >
         <Typography variant="button">IMPRESSIONS</Typography>
        </button>
        <button
          className={tab === 2 ? 'bg-selected width2' : 'width2'}
          onClick={() => setTab(2)}
        >
          <Typography variant="button">CLICKS</Typography>
        </button>
        <button
          className={tab === 3 ? 'bg-selected width2' : 'width2'}
          onClick={() => setTab(3)}
        >
          <Typography variant="button">COST</Typography>
        </button>
        <button
          className={tab === 4 ? 'bg-selected width1' : 'width1'}
          onClick={() => setTab(4)}
        >
          <Typography variant="button">CTR</Typography>
        </button>
</div>
<div>
        <button className={chart === false ? 'bg-chart width1' : 'bg-chart width1'} onClick={() => setChart(!chart)}
        >
          <Typography variant="button">{chart?"Bar Chart":"Line Chart"}</Typography>
        </button>
        </div>
      </div>

      <div id="system" style={{ display: tab === 1 ? 'block' : 'none' }}>
        {chart?<ChartComponent data={data1}/>:<BarChartComponent data={data1}/>}
      </div>

      <div id="adp" style={{ display: tab === 2 ? 'block' : 'none' }}>
        {chart?<ChartComponent data={data2}/>:<BarChartComponent data={data2}/>}
      </div>

      <div id="cdp" style={{ display: tab === 3 ? 'block' : 'none' }}>
        {chart?<ChartComponent data={data3}/>:<BarChartComponent data={data3}/>}
      </div>

      <div id="compliance" style={{ display: tab === 4 ? 'block' : 'none' }}>
        {chart?<ChartComponent data={data4}/>:<BarChartComponent data={data4}/>}
      </div>
    </Container>
    );
}

