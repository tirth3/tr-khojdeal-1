import styled from 'styled-components';
export const Container = styled.div`
  
  width: 100%;
  font-size: 14px;
  
  border-radius: 4px;
  .tab{
    display: flex;
    justify-content: space-between;
  }
  .bg-selected{
    background-color:#e0e5fd;
    border-bottom:2px solid #4c60cc;
  }
  .bg-chart{
        background-color:#e0e5fd;
    border:2px solid #4c60cc;
  }
  button{
    height: 50px;
    margin:0px;
    padding: 8px 4px;
    float: left;
    border: none;
    display: flex;
    justify-content: center;
    outline: 0;
    align-items: center;
  }
  .width1{
    width: 175px;
  }
  .width2{
    width: 200px;
  }
  .cs{
    width:auto;
    text-align:center;
    font-size:20px;
    margin-top:50px;
  }
  .link:hover{
    text-decoration: underline;
  }
`;
