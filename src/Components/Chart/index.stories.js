import React from 'react';

import Chart from '.';

export default {
  title: 'Chart',
  component: Chart,
  argTypes: { onClick: { action: () =>  alert("Clicked")} },
};

const Template = (args) => <Chart {...args} />;

export const Container = Template.bind({});
Container.args = {
 title:"Hi"
};