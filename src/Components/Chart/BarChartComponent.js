import { Tooltip } from '@material-ui/core';
import React from 'react';
import { BarChart, Bar,ResponsiveContainer, CartesianGrid, XAxis, YAxis, Legend } from 'recharts';


export default function BarChartComponent({data}){
    return (
      <ResponsiveContainer width="95%" height={400}>
        <BarChart width={150} height={40} data={data}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
          <Bar dataKey="uv" fill="#8884d8" />
        </BarChart>
      </ResponsiveContainer>
    );
}
