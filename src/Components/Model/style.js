import styled from "styled-components";

export const Container = styled.div`
  top: 60px;
  flex: 1 0 auto;
  bottom: 0px;
  display: flex;
  outline: 0;
  z-index: 1200;
  position: fixed;
  overflow-y: auto;
  flex-direction: column;
  left: auto;
  right: 0;
  background-color: #fff;
  box-shadow: 0px 8px 10px -5px rgb(0 0 0 / 20%),
    0px 16px 24px 2px rgb(0 0 0 / 14%);
  .form {
    display: flex;
    place-items: center;
    flex-flow: column;
  }
  .my-3 {
    margin-top: 3vh;
    margin-bottom: 3vh;
  }
  .border {
    border: 1px solid #ccc;
    border-radius: 4px;
    padding: 4px;
  }
  .flex {
    display: flex;
  }
  .btn {
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 0.25rem;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
      border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }
  .sticky-btn {
    display: flex;
    position: fixed;
    bottom: 0px;
    width: 100%;
  }
  .sticky-btn > button {
    width: 125px;
  }
  .btn-text {
    font-size: 1.4rem;
    text-align: center;
    width: inherit;
  }
`;
