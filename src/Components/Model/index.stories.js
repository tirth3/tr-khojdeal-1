
import React from 'react';
import Model from '.';

export default {
  title: 'Model',
  component: Model,
  argTypes: { onClick: { action: () =>  alert("Clicked")} },
};

const Template = (args) => <Model {...args} />;

export const Container = Template.bind({});
Container.args = {
togglemodel:false,
};