import { Button, Snackbar, TextField, Typography } from "@material-ui/core";
import React,{ useState } from "react";
import { Container } from "./style";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Model({ togglemodel }) {
  const [toggle, setToggle] = useState(false);
  const [acctoken, setAccToken] = useState();
  const [accid, setAccId] = useState();
  const [appid, setAppId] = useState();
  const [appsec, setAppSec] = useState();
  const [open, setOpen] = useState(false);


  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };
  const formSubmit = (event) => {
    event.preventDefault();
    localStorage.setItem("acctoken",acctoken);
    localStorage.setItem("accid", accid);
    localStorage.setItem("appid", appid);
    localStorage.setItem("appsec", appsec);
    setOpen(true);
  };
    const handleClear = (e) => {
      localStorage.clear();
      window.location.reload();
    };

  return (
    <Container
      style={{
        display: togglemodel !== toggle ? "none" : "block",
      }}
    >
      <div style={{ width: "250px", display: "flex" }}>
        <div>
          <Button variant="contained" onClick={() => setToggle(!toggle)}>
            Back
          </Button>
        </div>
        <Typography className="btn">Setting</Typography>
      </div>
      <form>
        <div style={{ width: "250px" }}>
          <div className="form">
            <TextField
              id="outline-multiline-static"
              label="Access Token"
              multiline
              rows={4}
              className="my-3"
              defaultValue={localStorage.getItem("acctoken")}
              onChange={(e) => setAccToken(e.target.value)}
              variant="outlined"
            />
            <TextField
              className="my-3"
              defaultValue={localStorage.getItem("accid")}
              onChange={(e) => setAccId(e.target.value)}
              id="standard-basic"
              label="Account ID"
            />
            <TextField
              className="my-3"
              defaultValue={localStorage.getItem("appid")}
              onChange={(e) => setAppId(e.target.value)}
              label="APPID"
            />
            <TextField
              className="my-3"
              defaultValue={localStorage.getItem("appsec")}
              onChange={(e) => setAppSec(e.target.value)}
              label="APP SECRET"
            />
          </div>
        </div>
        <div className="sticky-btn">
          <Button variant="contained" onClick={handleClear}>Clear</Button>
          <Button onClick={formSubmit} variant="contained" color="secondary">
            Save
          </Button>
          <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              {`Form Submitted!`}
            </Alert>
          </Snackbar>
        </div>
      </form>
    </Container>
  );
}
