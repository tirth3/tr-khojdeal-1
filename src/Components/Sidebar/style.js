import styled from "styled-components";

export const Side = styled.div`
  border-top-right-radius: 50px;
  border-bottom-right-radius: 50px;
  box-shadow: rgb(0 0 0 / 20%) 0px 3px 3px -2px,
    rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px;
  .grid {
    display: grid;
  }

  li {
    list-style-type: none;
  }
  .bg-selected {
    background-color: #ec0a71;
    color: #fff;
    border-color: #ec0a71;
    text-decoration: none;
    padding: 3vh 3vw;
    text-align:center;
  }
  .bg-normal {
    background-color: white;
    color: black;
    text-decoration: none;
    padding: 3vh 3vw;
    text-align:center;
  }
  .tr-radius {
    border-top-right-radius: 10px;
  }
  .br-radius {
    border-bottom-right-radius: 10px;
  }
  .collapse-btn {
    display: grid;
  }
  .collapse-btn div {
    width: 25px;
    height: 5px;
    background-color: black;
    margin-bottom: 2px;
  }
`;
