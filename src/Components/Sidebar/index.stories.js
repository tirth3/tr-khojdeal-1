import React from 'react';
import { BrowserRouter} from 'react-router-dom';
import Sidebar from '.';

export default {
  title: 'Sidebar',
  component: Sidebar,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <BrowserRouter><Sidebar {...args} /></BrowserRouter>;

export const Side = Template.bind({});
Side.args = {
togglevalue:true,
};