import React,{ useState } from 'react';
import { Link } from 'react-router-dom';
import { Side } from './style';
import Icon from '../../assets/abc.svg'
function Sidebar({togglevalue}){
  const [active,setActive]=useState(0);
  // const [props.togglevalue,setprops.togglevalue]=useState(true);
  return (
    <Side style={{ width: togglevalue ? "80px" : "160px" ,height:"100vh"}}>
      <div className="grid">
     
        <Link
          to="/Home"
          className={active === 0 ? "bg-selected tr-radius" : "bg-normal tr-radius"}
          onClick={() => setActive(0)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Dashboard"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 1 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(1)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Monthly Review"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 2 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(2)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Rewards"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 3 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(3)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Pip"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 4 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(4)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Link 4"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 5 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(5)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Link 5"
          )}
        </Link>
        <Link
          to="/Home"
          className={active === 6 ? "bg-selected" : "bg-normal"}
          onClick={() => setActive(6)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Link 6"
          )}
        </Link>
        <Link
          to="/Home"
          className={
            active === 7 ? "bg-selected br-radius" : "bg-normal br-radius"
          }
          onClick={() => setActive(7)}
        >
          {togglevalue ? (
            <img src={Icon} alt="" width="16px" height="16px" />
          ) : (
            "Link 7"
          )}
        </Link>
      </div>
    </Side>
  );
}
export default Sidebar;