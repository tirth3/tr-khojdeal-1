import { Typography } from "@material-ui/core";
import { Container } from "./style";
import React from "react";
function Footer(){
    return (
      <Container>
         <Typography variant="h4">
           Khojdeal &copy; 2020-2021
         </Typography>
      </Container>
    );
}
export default Footer;