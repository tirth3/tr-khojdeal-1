import React from 'react';
import Footer from '.';

export default {
  title: 'Footer',
  component: Footer,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

const Template = (args) => <Footer {...args} />;

export const Side = Template.bind({});
Side.args = {
togglevalue:true,
};