import styled from "styled-components";

export const Container = styled.div`
  bottom: 0;
  width: 100%;
  position: fixed;
  background-color: #e30a6c;
  text-align: center;
  box-shadow: rgb(0 0 0 / 20%) 0px 3px 3px -2px,
    rgb(0 0 0 / 14%) 0px 3px 4px 0px, rgb(0 0 0 / 12%) 0px 1px 8px 0px;
`;
