import {RecoilRoot} from 'recoil';
import { BrowserRouter, Route} from "react-router-dom";
import Main from './public/Main';
import React from "react";
function App() {
  return (
    <RecoilRoot>
      <BrowserRouter>
       <Route exact path="/" component={Main}/>
      </BrowserRouter>
    </RecoilRoot>
  );
}

export default App;
