import {atom} from "recoil";
const textState=atom({
    key:"textState",
    default:"Tirth"
})
const authState=atom({
    key:"auth",
    default:false
})

// const loginAuthState = selector({
//   key: "loggedin", // unique ID (with respect to other atoms/selectors)
//   get: ({ get }) => {
//     const text = get(authState);
//     return typeof(text);
//   },
// });

// export {loginAuthState};
export {textState};
export {authState};