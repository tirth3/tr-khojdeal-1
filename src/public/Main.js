import { useRecoilValue } from 'recoil';
import Layout from '../pages/Layout/index';
import Login from '../pages/Login';
import { authState } from '../State/State';
import React from "react";
import './login.css';

function Main(){
    const loggedin=useRecoilValue(authState);
    return loggedin?<Login/>:<Layout/>;
}
export default Main;